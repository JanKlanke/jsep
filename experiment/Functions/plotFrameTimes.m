function p = plotFrameTimes(tAll, tEnd)
%
% 2021 by Jan Klanke
% 
% Input:  tAll - Vector with timestamps for ALL frames
%         tEnd - time stamp immediatly after last frame
%
% Output: p - plotted results

global scr    

sdVec = diff(1000*(tAll-tEnd)); sdMean = mean(sdVec,'omitnan'); l = length(sdVec); fn= 1:length(tAll) - 1;

p = figure;
set(gcf,'position', get(0,'ScreenSize'));
hold on
title('\fontsize{20}drawShaderDemo: Frametime evaluation of phase shift vel. demo');
xlabel('\fontsize{16}No. of frame rel. last [n]');
ylabel('\fontsize{16}Time difference [ms]');
plot(fn, sdVec, fn, repmat(sdMean,1,l),'b--', fn,repmat(scr.fd*1000,1,l),'r--');
legend('time between frames [ms]', 'mean time diff. between frames [ms]', 'optimal frame dur.[ms]');
hold off
end