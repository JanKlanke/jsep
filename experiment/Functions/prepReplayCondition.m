function [ms, start, abort] = prepReplayCondition(vpno, seno, coow, start)

% reads in the various data for the ms conditions and makes sure that
% everything is as it should be
% input: vp number, session number
% output: array with ms coords, velocities, onsets, and fitted main
% sequence parameters
%
% 06/2020 by Jan Klanke

% stopping vat
abort = 0;

% function to easily filter the names of the files for a certain pattern.
sf = @(cll,ptn) ~cellfun('isempty', regexp(cll,ptn));

% get last session no of completed sessions
if coow; sesnum = 0;
else sesnum = str2double(seno) - 1; end

% predefine ms structures
ms  = struct('info',[],'mnsq',[]);
 
% filter files of participant
mnsqcsFilesAll = dir(sprintf('Data/mainSequences/%s*_mSequ.csv',vpno));
coordsFilesAll = dir(sprintf('Data/msCoordinates/%s*_msCoords.csv',vpno));

% switch for session number
switch sesnum
    case 0
        fprintf(1,'\n>>>> Saccade data status: no saccade data');
        fprintf(1,'\n>>>> ...proceed...\n');
        WaitSecs(0.5); 
        start = 0;
        
    otherwise
        % search for files and select relevant subset\
        prev_sesnum = 1:sesnum;
        prev_sesnam= arrayfun(@(x) sprintf('%s%02d',vpno, x ), prev_sesnum, 'UniformOutput', false);
        prev_sesidx = sf({coordsFilesAll.name},strjoin(prev_sesnam, '|'));       % index of sessions of this vp
        cfiles = coordsFilesAll(prev_sesidx);                                    % files cleaned by session
        
        % communicate successful find
        if isempty(cfiles)
            fprintf(1,'\n>>>> Saccade data status: NOT available...'); 
            fprintf(1,'\n>>>> ...abort...\n'); 
            abort = 1; return;
        else
            fprintf(1,'\n>>>> Saccade data status: available...'); 
        end
        
        % load in ms data of all available files
        msStruct = cellfun(@(x) importdata(x, ','), {cfiles(sf({cfiles.name}, 'msCoords')).name});
        ms.info= vertcat(msStruct.data);
        ms.mnsq= importdata(mnsqcsFilesAll.name);
        
        % communicate successful load
        while ~any(structfun(@isempty, ms))
                % show files
                fprintf(1,'\n>>>> I was able to load these files...');
                fprintf(1,'\n>>>> (*) %s (created %s)', (mnsqcsFilesAll.name), extractBefore(mnsqcsFilesAll.date, ' '));
                fprintf(1,'\n>>>> (*) %s (created %s)', [{cfiles.name}', extractBefore({cfiles.date}, ' ')']');
                o = input('\n\n>>>> Do you want to continue [y / n]? ','s');
                
                % allow experimenter to decide whether they want to
                % continue
                if strcmp(o,'y')
                    fprintf(1,'>>>> ...proceed...\n');
                    WaitSecs(0.5); 
                    start = 0; break;
                else
                    ms = struct('coords',[]);
                end
        end
        if any(structfun(@isempty, ms))
            fprintf(1,'\n>>>> I was not able to load these files...');
            fprintf(1,'\n>>>> ...abort...\n');
            abort = 1; return;
        end
end

end

