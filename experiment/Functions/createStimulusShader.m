function texture = createProceduralStimulus(imgMat)
% 2021 by Jan Klanke
% 
% Input:  imgMat  - matrix of the image--usually of the form: [image : image : alphaVals]
%
% Output: texture - texture pointer

global scr 
  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define shader and convert image matrix into texture %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% make sure this GPU supports shading at all
AssertGLSL;

% activate alpha blending
Screen('BlendFunction', scr.main, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

% create texture shader
glsl = MakeTextureDrawShader(scr.main, 'SeparateAlphaChannel'); 

% make image matrix into texture
texture = Screen('MakeTexture', scr.main, imgMat, [], [], [], [], glsl);

% finally, disable alpha blending for main screen again
Screen('BlendFunction', scr.main, GL_ONE, GL_ZERO);