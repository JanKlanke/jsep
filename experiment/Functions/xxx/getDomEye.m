function [domEye] = getDomEye
% 2019 by Jan Klanke

default = ['L', 'R', 'B'];
domEye = NaN;

% ask for conditions to be displayed
while length(intersect(domEye, default)) < length(domEye)
    domEye = input('>>>> Enter dominant eye [e.g. L, R, B]:  ','s');
    if isempty(domEye)
        domEye = 'R';
    end
   
    if length(intersect(domEye, default)) < length(domEye)
        fprintf(1, 'WARNING:  You made a mistake when trying to specify the dominant eye. \nWARNING:  You can choose between the following options: L, R, B\n');
    end
end