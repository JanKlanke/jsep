function [data,eyeData] = runSingleTrial(td)
% 2016 by Martin Rolfs 
% 2021 by Jan Klanke
% 
% Input:  td      - part of the design structure that pertains to the
%                   trial to be displayed
% 
% Output: data    - trial parameters and behavioral results
%         eyeData - flag for eyetracking  

global scr visual setting re keys

% clear keyboard buffer
FlushEvents('KeyDown');
if ~setting.TEST == 1, HideCursor(); end
pixx = setting.Pixx;

% Set the transparency for the stimulus (Gabor patch or sine w/ raised
% cosine masketc.)
% Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
Screen('BlendFunction', scr.myimg, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

% predefine boundary information
cxm = td.fixa.loc(1);
cym = td.fixa.loc(2);
rad = visual.boundRad;
chk = visual.fixCkRad;

% draw trial information on operator screen
if ~setting.TEST, Eyelink('command','draw_box %d %d %d %d 15', (cxm-chk)*2, (cym-chk)*2, (cxm+chk)*2, (cym+chk)*2); end

% % generate donut shaped stimulus
nStim = length(td.stims.pars.sizp);
sti.tex = visual.procStimsTex ;
sti.vis = td.stims.pars.sizp;
sti.src = [zeros(2,nStim); sti.vis; sti.vis]';
sti.dst = CenterRectOnPoint(sti.src, td.stims.locX, td.stims.locY);
for f = 1:td.totNFr; stiFrames(f).dst = sti.dst + repmat(td.posVec(:,f),2,nStim)'; end

% predefine time stamps
tStimOn = NaN;  % t of stimulus stream on
tStimCf = NaN;  % t of stimulus at full contrast
tStimMS = NaN;  % t of stimulus starting to move
tStimMT = NaN;  % t of stimulus at peak velocity
tStimCd = NaN;  % t of stimulus contrast starts to decline
tStimOf = NaN;  % t of stimulus off
tRes    = NaN;  % t of response (if any)
tClr    = NaN;  % t of of clear screen

% set flags before starting stimulus stream
eyePhase  = 1;  % 1 is fixation phase, 2 is saccade phase
breakIt   = 0;
fixBreak  = 0;
saccade   = 0;

% eyeData
eyeData = [];

% Initialize vector important for response
pressed = 0;
resTier = 1;   
response= NaN(1,3);

% Initialize vector to store data on timing
frameTimes = repmat(12*scr.fd,1,td.totNFr);

% flip screen to start out time counter for stimulus frames
firstFlip = 0;nextFlip = 0;
while firstFlip == 0
    if pixx, firstFlip = PsychProPixx('QueueImage', scr.myimg);
    else     firstFlip = Screen('Flip', scr.myimg); end
end

% set frame count to 0
f = 0;              

% get a first timestap
tLoopBeg = GetSecs; 
t = tLoopBeg;

while ~breakIt && f < td.totNFr
    f = f+1;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % stimulus presentation %
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
    for slo = 1:setting.sloMo
        Screen('FillRect', scr.myimg, visual.bgColor);
        % stimulus
        % Screen('BlendFunction', scr.myimg, GL_SRC_ALPHA, GL_ONE);
        if td.stims.vis(f)
            Screen('DrawTexture', scr.myimg, sti.tex, sti.src, stiFrames(f).dst, td.stims.pars.ori, [], td.stims.amp(:,f), [], [], [], [0, td.stims.pha(:,f), 0, 0]);
        end
        % flip
        if pixx; nextFlip = PsychProPixx('QueueImage', scr.myimg);
        else     nextFlip = Screen('Flip', scr.myimg); end
    end
    frameTimes(f) = frameTimes(f) + GetSecs;   
    
    %%%%%%%%%%%%%%%%%%%%%%%%
    % raise stimulus flags %
    %%%%%%%%%%%%%%%%%%%%%%%%
    % Send message that stimulus is now on
    if isnan(tStimOn) && td.events(f)==1
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimOn'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimOn'); end
        tStimOn = frameTimes(f);
    end
    if isnan(tStimCf) && td.events(f)==2
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimCf'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimCf'); end
        tStimCf = frameTimes(f);
    end
    if isnan(tStimMS) && td.events(f)==3
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimMS'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimMS'); end
        tStimMS = frameTimes(f);
    end
    if isnan(tStimMT) && td.events(f)==4
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimMT'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimMT'); end
        tStimMT = frameTimes(f);
    end
    if isnan(tStimCd) && td.events(f)==5
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimCd'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimCd'); end
        tStimCd = frameTimes(f);
    end
    if isnan(tStimOf) && td.events(f)==6
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimOf'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimOf'); end
        tStimOf = frameTimes(f);
    end
    
    % eye position check
    if setting.TEST<2
        [x,y] = getCoord;
        if sqrt((x-cxm)^2+(y-cym)^2)>chk    % check fixation in a circular area
            fixBreak = 1;
        end
    end
    if fixBreak
        breakIt = 1;    % fixation break
    end
end

lastFlip = 0;
while lastFlip == 0 && nextFlip == 0
    Screen('FillRect', scr.myimg, visual.bgColor);
    if pixx, lastFlip = PsychProPixx('QueueImage', scr.myimg);
    else     lastFlip = Screen('Flip', scr.myimg); end 
end
tLoopEnd = GetSecs;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% stimulus presentation 'Blank' %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
for i = 1:scr.rate
    Screen('FillRect', scr.myimg, visual.bgColor);
    if pixx, PsychProPixx('QueueImage', scr.myimg);
    else     Screen('Flip', scr.myimg); end
end
WaitSecs(td.aftKey/4);

% Init rotary encoder, reset if necessary
if setting.knob
    ticksRaw = re.getStatus();
    if ticksRaw ~= 0, re.resetEncoder(); end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Break trial or initiate reponse query %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nextRespFlip = 0; lastRespFlip = 0;
switch breakIt
    case 1
        data = 'fixBreak';
        if setting.TEST<2, Eyelink('command','draw_text 100 100 15 Fixation break'); end
    otherwise
        
        % Set init parameters
        tResBeg = GetSecs;ticksOld = 0;delta=0;ticks = td.sOnPos;
        
        % response phase
        while ~pressed   
            
            % evaluate response to first 2 questions
            if resTier <= 2
                if ( td.scale.Dir && checkTarPress(keys.resRghtA)) || (~td.scale.Dir && checkTarPress(keys.resLeftA)) 
                    response(resTier) = 1; 
                    resTier = resTier + 1; 
                    WaitSecs(td.aftKey);
                end
                if (~td.scale.Dir && checkTarPress(keys.resRghtA)) || ( td.scale.Dir && checkTarPress(keys.resLeftA))
                    response(resTier) = 0; 
                    resTier = resTier + 1; 
                    WaitSecs(td.aftKey); 
                end
            end

            % evaluate response to last 2 questions
            if resTier > 2
                 
                % number of available items
                 n = size(td.scale.lvl(resTier).a.labels,2);   % td.scale.itemN-1 
    
                % evaluate first 2 responses
                if response(2) == 0 && resTier == 3; resTier = resTier + 1;end
                if response(1) == 0 && ~isnan(response(2)); pressed = 1; end  % skip response tier >2 if no grating has been seen
                if response(1) == 1 && ~isnan(response(2))
                    % evaluate responses (make ticks correspond to scale from 0 to 3
                    % with 1 = not sure, 2 = rather unsure, 3 = rather sure, and 4 = sure.
                    if td.scale.Dir; response(3) = ticks; else response(3) = n - ticks + 1; end
                    pressed = checkTarPress(keys.resSpace);      
                end
                % break loop when space key is pressed
                if pressed; continue; end
                
                % get rotary encoder and response tick(s)
                if setting.knob
                    [ticksRaw, pressed] = re.getStatus();
                    if ticksRaw ~= ticksOld
                        delta = ticksOld - ticksRaw; 
                        ticksOld = ticksRaw;
                        ticks = ticks + delta;                      % add onset position of scale hand and flip ticks
                    end
                    % normale ticks between 1 and 4
                    ticks(ticks <= 0) = 0;                          % make sure that ticks are never below zero
                    ticks(ticks > td.scale.Speed) = td.scale.Speed; % make sure that ticks are never above max response option   
                    
                    % Recalculate ticks for display
                    ticks= round(ticks/(td.scale.Speed/(n-1)))+1;
                else
                    delta = delta + checkTarPress(keys.resRghtA) - checkTarPress(keys.resLeftA);
                    if abs(delta) >= 5; ticks = ticks + sign(delta); delta = 0; end
                    ticks(ticks< 1) = 1; ticks(ticks > n) = n;
                end
            end
            
            % create response graphics for scale
            for i = 1:scr.rate
                Screen('FillRect', scr.myimg, visual.bgColor);         % draw background
                drawScale(ticks, td.scale, resTier);
                if pixx, nextRespFlip = PsychProPixx('QueueImage', scr.myimg);
                else     nextRespFlip = Screen('Flip', scr.myimg); end
            end
            tRes = GetSecs(); 
        end
        
        % Fill up buffer 
        lastRespFlip = 0;
        while lastRespFlip == 0 && nextRespFlip == 0
            Screen('FillRect', scr.myimg, visual.bgColor);
            if pixx, lastRespFlip = PsychProPixx('QueueImage', scr.myimg);
            else     lastRespFlip = Screen('Flip', scr.myimg); end 
        end
        
        % make one flip with empty screen
        for i = 1:scr.rate
            Screen('FillRect', scr.myimg, visual.bgColor);
            if pixx, PsychProPixx('QueueImage', scr.myimg);
            else     Screen('Flip', scr.myimg); end
        end
        
        % reset encoder 
        if setting.knob; re.resetEncoder(); end
        
        % determine response duration 
        tResDur = (tRes - tLoopBeg) * 1000; 
                             
        WaitSecs(td.aftKey);       
        tClr = GetSecs;
        if setting.TEST<2, Eyelink('message', 'EVENT_Clr'); end
        if setting.TEST; fprintf(1,'\nEVENT_Clr'); end 
        
        % plot frame times.
        % plotFrameTimes(frameTimes, tLoopEnd)
        
        %-------------------------%
        % PREPARE DATA FOR OUTPUT %
        %-------------------------%
        % collect trial information
        condData = sprintf('%i\t%i',...
            [td.expcon setting.train]);            % in results, cells  6:8
        
        % collect stimulus information                                        in results, cells 9:20
        stimData = sprintf('%.2f\t%.2f\t%i\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%i\t%.2f\t%.2f',...  
            [td.fixpox td.fixpoy td.stiori td.phavel td.appbeg td.apptop td.appdur td.appmdX td.appmdY td.appori td.appvpk td.appamp]);
        
        % collect data on the artificial saccade that shaped the stim.        in results, cells 21:26
        asacData = sprintf('%i\t%i\t%.2f\t%.2f\t%.2f\t%.2f',...
            [td.sac.idx td.sac.req td.sac.amp td.sac.vpk td.sac.dur td.sac.ang]);
        
        % collect scale information                                           in results, cells 27:31
        scaleData = sprintf('%i\t%i',...
            [td.scale.Dir td.sOnPos]); 
                           
        % collect time information                                            in results, cells 32:39
        timeData = sprintf('%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i',...                             
            round(1000*([tStimOn tStimCf tStimMS tStimMT tStimCd tStimOf tRes tClr]-tStimOn)));
        
        % collect response information                                        in results, cells 40:43
        respData = sprintf('%i\t%i\t%i',... 
            response);      

        % get information about how timing of frames worke                    in results, cells 44:45
        tLoopFrames = round((tLoopEnd-tLoopBeg)/scr.fd);        
        frameData = sprintf('%i\t%i',tLoopFrames,td.totNFr);

        % collect data for tab [3 x condData, 10 x trialData, 5 x sacData, 10 x timeData %i, 8 x respData, 2 x frameData]
        data = sprintf('%s\t%s\t%s\t%s\t%s\t%s\t%s',...
            condData, stimData, asacData, scaleData, timeData, respData, frameData);
end
end