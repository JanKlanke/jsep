function base = raisedCos2D(siz,R)

base = zeros(siz,siz);
tukey = tukeywin(siz,R);
tukey = tukey(siz/2:siz);
x = linspace(-siz/2, siz/2, siz);
y = linspace(-siz/2, siz/2, siz);
for i=1:1:siz
    for j=1:1:siz
        if (round(sqrt(x(i)^2 + y(j)^2)) <= siz/2)
            round(sqrt(x(i)^2+y(j)^2));
            base(i,j) = tukey(round(sqrt(x(i)^2+y(j)^2)));
        end
    end
end