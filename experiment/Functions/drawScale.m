function drawScale(p, scl, rT)
% 2020 by Jan Klanke
% 
% Input:  p   - index of the position at which the scale hand should be drawn
%         scl - parameters of the scale
%         rT  - response tier; i.e. which question/response level is displayed

global scr

% Do stuff for scale if the response tier is >2
if rT > 2
    % normalize p to vector length
    pVec = 0:scl.Speed/(size(scl.lvl(rT).a.labels,2)-1):scl.Speed;
    p = pVec(p);
    
    % Ensure that p cannot be 0.
    p(p==0) = p+1;

    % Adapt locations in case more than one clock hand needs to be drawn.
    if scl.handN>1, p = p:length(scl.hand.posvecP)/scl.handN:length(scl.hand.posvecP) + p-1; end

    % Correct for entries that are too big.
    p(p>length(scl.hand.posvecP)) = p(p>length(scl.hand.posvecP)) - (length(scl.hand.posvecP)*floor(p(p>length(scl.hand.posvecP))/length(scl.hand.posvecP)));

    % Re-size vector with x and y columns of line elements.
    vec = scl.hand.posvecP(:,p);

    % Draw scale and scale hand.
    Screen('DrawLines', scr.myimg, scl.line.pos, scl.widthP, scl.bandColV);
    Screen('DrawDots',  scr.myimg, vec, scl.handSizeP, scr.white, [], 1);
end


% Draw labels for the scale.
scl.lvl(rT).a.labels = reshape(scl.lvl(rT).a.labels',1,[]);
for i= 1:numel(scl.lvl(rT).q.labels); drawText(scl.lvl(rT).q.labels{i},scl.lvl(rT).q.posP(i,1),scl.lvl(rT).q.posP(i,2)); end
for i= 1:numel(scl.lvl(rT).a.labels); drawText(scl.lvl(rT).a.labels{i},scl.lvl(rT).a.posP(i,1),scl.lvl(rT).a.posP(i,2)); end

end