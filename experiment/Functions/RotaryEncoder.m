classdef RotaryEncoder < handle
    
    properties
        arduino
    end
    
    methods
        
        function self = RotaryEncoder(port)
            self.connect(port);
        end
        
        function connect(self, port)
            % Check whether ACM port is recognized under 'port'
            if isunix
                
                % Get info on available ports
                avaSP = instrhwinfo('serial');
                fprintf(1, 'Checking availability of ACM port under %s ... \n', port);
                
                % Check whether symlink port is available AND create if the
                % symlink if necessary.
                while ~any(strcmp(avaSP.AvailableSerialPorts, port))
                    commandwindow;
                    fprintf(1, 'Matlab currently fails to recognize Arduino''s port.\nI can try to resolve this issue by creating a symlink but I need your authorization: Pls enter this user''s pw below:\n');
                    [status, result] = system(['sudo ln -s /dev/ttyACM* ', port]);
                    
                    % Check whether the installation of the symlink was
                    % successfull.
                    if status 
                        fprintf(1, '\nI failed to establish the symlink.\n');
                        fprintf(1, 'System message: %s\n', result); 

                        % Communicate option to remove and reinstall the
                        % symlink
                        fprintf(1, 'I can remove this symlink and retry the installation.\nIf you have tried this already there is a deeper issue and I recommend aborting this process.\n');
                        o = input('Do you want me to remove the old symlink and try to re-installit [y / n]? ','s');
                        if strcmp(o,'y')
                            [~, ~] = system(['sudo rm ', port]);
                        else
                            fprintf(1, 'You have decided to abort the reinstallation. Abort mission.\n\n');
                            WaitSecs(.2); return;
                        end
                    end
                    
                    avaSP = instrhwinfo('serial');
                    if any(strcmp(avaSP.AvailableSerialPorts, port))
                        fprintf(1, '\nPort available.\n');
                    end
                end
            end
            
            % Configure serial port
            self.arduino = serial(port, 'BaudRate', 115200);
            fprintf(1,['Trying to connect to Arduino at "', port, '"...\n']);
            try
                fopen(self.arduino);
            catch ME
                disp('Connection could not be established.');
                rethrow(ME);
            end
            % Wait for serial interface of Arduino to be ready
            WaitSecs(1);
            fprintf(1,'Connection established.\n');
        end
        
        function disconnect(self)
            fclose(self.arduino);
        end
        
        function delete(self)
            self.disconnect();
        end
        
        function requestStatus(self)
            fprintf(self.arduino, 1);
        end
        
        function [encoder, button] = receiveStatus(self)
            data = fscanf(self.arduino, '%d %d');
            encoder = data(1);
            button= data(2);
        end
        
        function [encoder, button] = getStatus(self)
            self.requestStatus();
            [encoder, button] = self.receiveStatus();
        end
        
        function resetEncoder(self)
            fprintf(self.arduino, 2);
        end
        
    end
end

