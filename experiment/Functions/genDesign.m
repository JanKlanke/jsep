function design = genDesign(vpno, seno, cond, dome, ms, subjectCode)
% 2021 by Jan Klanke
% 
% Input:  vpno        - id of the participant
%         seno        - number of the session of the participant
%         cond        - number of the condition/s to be presented
%         dome        - dominant eye
%         ms          - array with microsaccade data for replay
%         subjectCode - subject code (experiment name + vpno + seno)
% 
% Output: design      - array with design structure of the experiment

global visual scr keys setting %#ok<NUSED>

% strictly for code-testing: Emulation of propixx on low res screen: 
if setting.Pixx == 2; scr.refr = 1440; scr.fd = 1/scr.refr; end

% randomize random
rand('state',sum(100*clock));

% function for rounding away from zero
paren = @(x, varargin) x(varargin{:});
ceilfix = @(x)ceil(abs(x)) .* sign(x);

% this subject's main sequence parameters
V0 = 450;A0 = 7.9;durPerDeg = 2.7;durInt = 23;

% participant parameters
design.vpno = vpno;
design.seno = str2double(seno);
design.dome = dome;

% no. of blocks and trials per condition
if setting.train
    design.nBlocks = 1;                         % number of blocks
    design.nTrialsPerCellInBlock = 25;          % number of trials per cell in block
else
    design.nBlocks = 2;                        % number of blocks
    design.nTrialsPerCellInBlock = 8;           % number of trials per cell in block
end

% condition parameter
expstruct = [1 1 repelem(2:length(cond),1,4)];
design.condRat = cond(expstruct);               % 1 = no stim, 2 = simulated microsaccade, 3 = active not-individualized microsaccade, 4 = microsaccade replay, 5 = active, individualized microsaccade
design.fixPres = [0 0 0 0 0];                   % 0 = no fix, 1 = fix present    
design.stiPres = [0 1 1 2 2];                   % 0 = no stim, 1 = stim present    
design.appShif = [0 1 0 2 0];                   % 0 = no apperture shift, 1 = apperture shift
design.sacRequ = [0 0 1 0 2];                   % 0 = no saccade, 1 = saccade
design.feBaPrs = setting.train;                 % 0 = no feedback, 1 = w. feedback
    
% setting for fixation (before cue)
design.timFixD = 0.000;                         % minimum fixation duration before stim onset [s]
design.timFixJ = 0.000;                         % additional fixation duration jitter before cue onset [s]

% timing settings for stimulus
design.timAfKe = 0.200;                         % recording time after keypress  [s]
design.timMaSa = 1.000;                         % time to make a saccade (most likely)  [s]
design.timStiD = design.timMaSa;                % stimulus stream duration    [s]
design.iti     = 0.000;                         % inter-trial interval [s]

% fixation dot parameters 
design.fixSiz = 0.1;      % size of the fixation dot (inner part) [dva]
design.gosSiz = 0.3;      % size of the fixation dot (outer part) [dva]                    

% stimulus parameters (artificial MS aperture shift)
design.numStim = 1;                                                        % number of stimuli
design.stiAngl = -pi:(2*pi/360):pi-(2*pi/360);                             % vector with possible stimulus orientations (0:359 deg)
design.phaVelDS= V0 * (1 - exp(-.5 / A0));                                 % default velocity of the phase shift velocity [dva/s]
design.appMoveEndpoint  = [cos(design.stiAngl); sin(design.stiAngl)];      % vector with the X and Y coordinates of the endpoints of the apperture movements. The Y coordinate is flipped: sin() * -1! 
design.appMoveIdx       = reshape(round(normrnd(0,25,1,100000)+[0;90;180;270]),1,[]);    % vector with indeces all of the possible apperture movements. Indceses are normally distributed
design.appMoveIdx(design.appMoveIdx>360) = 360-design.appMoveIdx(design.appMoveIdx>360); % around the cardnial directions: i.e. no of idx. peaks at 0, 90, 180, and 270 deg 
design.appMoveIdx(design.appMoveIdx<0)   = 360+design.appMoveIdx(design.appMoveIdx<0);   % and cupped at 0 and 360 deg
design.appMoveIdx(design.appMoveIdx==0)  = [];

% scale parameters
design.scale.handN  = 1;        % number of scale hands to be used 
design.scale.itemN  = 7;        % number of items on the likert scale 
design.scale.length = 15;       % scale length radius [dva]
design.scale.width  = .15;      % scale width radius [dva]
design.scale = getScalePars(design.scale); % generate scale based on parameters

% load in ms parameter
design.sac = getMsData(ms, vpno);
sacIdxList = 1:size(design.sac,2);

% pre drawing of trialcount
c = 0; 
totNT = design.nBlocks*design.nTrialsPerCellInBlock*length(design.condRat);

% loop through trials
for b = 1:design.nBlocks
    t = 0;
    for cond = design.condRat 
        for itri = 1:design.nTrialsPerCellInBlock
            t = t + 1; c = c + 1;
            fprintf(1,'\n preparing trial %i ...',t);
            trial(t).trialNum = t; %#ok<*AGROW>

            % fixation positions
            fixx = -4 + (4 + 4) * rand;   % eccentricity of fixation x (relative to screen center) - can be pos or neg [dva]
            fixy = -4 + (4 + 4) * rand;   % eccentricity of fixation y (relative to screen center) - can be pos or neg [dva]
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %      Saccade parameter in diff. conditions      %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Deault ms values (no-stimulus condition)
            trial(t).sac.idx = NaN;
            trial(t).sac.amp = 0;
            trial(t).sac.vpk = 0;
            trial(t).sac.dur = 0;
            trial(t).sac.ang = NaN;
                   
            % modifications if its the first session and theactive and the
            % repaly conditions are based on the data of a 'simulated' data
            if  design.appShif(cond) == 1
                                
                % REPlay Index: In the simulated microsaccade condition determined by pregenerated saccade szenarios.
                repi = ceilfix(design.appMoveIdx(ceilfix(length(design.appMoveIdx) * rand))); 
                trial(t).sac.index = repi;
                
                % Expected saccade velocities based on amplitudebars
                % (using equation and parameters used in Collewijn,
                % 1988). Angle of saccade to be replayed is based on
                % on the endpoints of the saccade that is replayed.
                trial(t).sac.amp = 0.5;                                         % in dva
                trial(t).sac.vpk = V0 * (1 - exp(-trial(t).sac.amp / A0));      % in dva/s (the actual peak velocity of the aperture motion is slightkly higher; around 35.5 dva/s
                trial(t).sac.dur = durPerDeg * trial(t).sac.amp + durInt;       % should be around 25 ms according to Martinez-Conde et al. 2004 [ms]
                trial(t).sac.ang = atan2(design.appMoveEndpoint(2, repi), design.appMoveEndpoint(1, repi)); % saccade orientation determined by predefined stimulus orientations [rad]                       
                
            elseif design.appShif(cond) == 2
                % REPlay Index: In microsaccade replay condition determined
                % by number of pre-recordded saccades.
                % for one-by-one check: repi = repi + 1; if repi > size(design.sac,2); repi = 1; end
                if isempty(sacIdxList); sacIdxList = 1:size(design.sac,2); end  % check whether list /w saccade indeces is empty and recreate it if so.
                repi = sacIdxList(ceil(rand*size(sacIdxList,2)));               % replay index
                sacIdxList(sacIdxList == repi) = [];                            % delete idx from list
                                
                % Saccade info
                % Every aspect of the replay-saccades is determined by
                % previously recorded saccades.
                trial(t).sac = design.sac(repi);                 % load spatial information of the saccade
                trial(t).sac.idx = repi;                         % store index of the saccade to be replayed.
                
                % add main sequence parameters
                trial(t).sac.amp = trial(t).sac.cVectorMS(end) - trial(t).sac.cVectorMS(1);          % in dva
                trial(t).sac.vpk = max(trial(t).sac.vVectorMS) * scr.refr;                           % in dva/s
                trial(t).sac.dur = length(trial(t).sac.cVectorMS) * scr.fd * 1000;                   % in ms 
                trial(t).sac.ang = atan2(trial(t).sac.coordsMS(2,end),trial(t).sac.coordsMS(1,end)); % saccade orientation that makes the stim. visible [rad]                                                  % Saccade IS required for the stimulus to be perceived 
            end
            
            % Determine whether the saccade is necessary to perceive the
            % stimulus.
            trial(t).sac.req = design.sacRequ(cond) > 0;
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %            Temporal trial settings              %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % duration before stimulus onset [frames]
            trial(t).fixNFr = round((design.timFixD + design.timFixJ * rand) / scr.fd);
            trial(t).fixDur = trial(t).fixNFr * scr.fd;

            % duration after stimulus onset [frames]
            trial(t).stiNFr = round(design.timStiD / scr.fd);
            trial(t).stiDur = trial(t).stiNFr * scr.fd;
            
            % duration of stimulus contrast ram [frames]
            trial(t).ramNFr = round((design.timStiD / 5) / scr.fd);
            trial(t).ramDur = trial(t).ramNFr * scr.fd;
            
            % timing parameters of apperture shift [frames]
            % (saccade duration is a good approximation of the duration of
            % the apperture shift)
            trial(t).shiNFr = round((trial(t).sac.dur / 1000) / scr.fd);       
            trial(t).shiDur = trial(t).shiNFr * scr.fd;
                       
            % calculate total stimulus duration [frames]
            trial(t).totNFr = trial(t).fixNFr + trial(t).stiNFr;

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Generate flag streams for stimulus presentation %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % time of the fixation            
            fixBeg = 1;
            fixEnd = fixBeg + trial(t).fixNFr;      
            
            % time that the stimulus is present
            stiBeg = trial(t).fixNFr + 1;
            stiEnd = stiBeg + trial(t).stiNFr - 1;
            
            % time that the stimulus is at full contrast
            ctrBeg = stiBeg + trial(t).ramNFr;                              
            ctrEnd = stiEnd - trial(t).ramNFr; 
            
            % begin and end of the time window in which the apperture MAY move
            shiBeg = ctrBeg + round(trial(t).shiNFr / 2);
            shiEnd = ctrEnd - round(trial(t).shiNFr / 2);
            
            % time parameter of apperture movement
            appBeg = NaN;appEnd = NaN;appTop = NaN;
            if design.appShif(cond) == 1
                % begin and end of the ACTUAL movement; moment at which 
                % the apperture velocity is highest
                appTop = shiBeg + round(rand * (shiEnd - shiBeg));   
                appBeg = appTop - round(trial(t).shiNFr / 2);
                appEnd = appBeg + trial(t).shiNFr - 1;
            elseif design.appShif(cond) == 2
                % begin and end of the actual apperture movement
                % NOT dependent on real onsets: appBeg = shiBeg + round((shiEnd - shiBeg) * rand);
                appBeg = round(trial(t).sac.onsetMS / 1000 * scr.refr);
                appEnd = appBeg + length(trial(t).sac.cVectorMS) - 1;
                appTop = appBeg + find(trial(t).sac.vpk == trial(t).sac.vVectorMS * scr.refr);
            end

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %              spatial information                %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % fixation
            trial(t).fixa.vis = zeros(1,trial(t).totNFr);
            trial(t).fixa.vis(fixBeg:fixEnd) = design.fixPres(cond);
            trial(t).fixa.loc = visual.scrCenter + round(visual.ppd * [fixx fixy fixx fixy]);
            trial(t).fixa.sin = round(design.fixSiz * visual.ppd);
            trial(t).fixa.sou = round(design.gosSiz * visual.ppd);
            trial(t).fixa.col = visual.black;
            
            % determine stimulus parameters
            % spatial stimulus settings (standard)
            trial(t).stims.locX = trial(t).fixa.loc(1); 
            trial(t).stims.locY = trial(t).fixa.loc(2);

            % determine stimulus parameters
            % spatial stimulus settings (standard)
            trial(t).stims.col  = repmat(visual.black,design.numStim,1);
            trial(t).stims.pars = getStimulusPars(design.numStim);
            if design.stiPres(cond) == 0; trial(t).stims.pars.ori = NaN;
            else trial(t).stims.pars.ori = paren([0 180], round(1+rand)); end
            
            % define change in stimulus position across frames
            appmdX=NaN;appmdY=NaN;appdur=NaN; appvpk= NaN;% defaults
            trial(t).posVec = zeros(design.numStim * 2,trial(t).totNFr); 
            
            % Stimulus position changes in the simulated MS and MS replay conditions:
            if design.appShif(cond) > 0
                if design.appShif(cond) == 1
                    % Calculate and correct replay index so that it the
                    % aperture moves in the OPPOSITE direction of the
                    % saccade. 
                    repi = 180 + repi;
                    repi(repi > 360) = repi(repi > 360) - 360;
                    
                    % Calculate changes in position vector (0:1).
                    trial(t).posVec(:,shiBeg:shiEnd) = repmat(normcdf(shiBeg:shiEnd, appTop, trial(t).sac.dur/3), 2, 1);
                    trial(t).posVec(:,shiEnd + 1:stiEnd) = repmat(trial(t).posVec(:,shiEnd),1,length(shiEnd + 1:stiEnd));

                    % Scale changes by actual saccade amplitude and
                    % rotate by angle. Also translates degrees to pixels. 
                    trial(t).posVec = (trial(t).sac.amp .* design.appMoveEndpoint(:,repi) .* trial(t).posVec) * visual.ppd;
                    
                elseif design.appShif(cond) == 2
                    % Translates coordinates of saccade vector into pixels.
                    trial(t).posVec(:,appBeg:appEnd)   = trial(t).sac.coordsRC * visual.ppd;  
                    trial(t).posVec(:,appEnd+1:stiEnd) = repmat(trial(t).posVec(:,appEnd),1,length(appEnd+1:stiEnd));         
                end   
                % Flip y dimension to account for the different
                % coordinate system of the screen
                trial(t).posVec(2,:) = trial(t).posVec(2,:) * -1;

                % Compute some chararteristics of the apperture
                % movement for the output
                appmdX = trial(t).posVec(1,appEnd) * visual.dpp;       % store final x coordinate for later storage [dva]
                appmdY = trial(t).posVec(2,appEnd) * visual.dpp;       % store final y coordinate for later storage [dva]           
                appdur = length(appBeg:appEnd);                        % calculate the duration of the apperture shift [ms]   
                
                % Get peak velocoty of aperture motion in dva/s.
                appvel = diff(trial(t).posVec, 1,2);
                appvpk = max(sqrt(appvel(1,:).^2 + appvel(2,:).^2) * scr.dpp * scr.refr);
            end

            % define modulation of top velocity across frames
            % (here, we are keeping stimulus velocity constant)
            velVec = ones(1,length(stiBeg:stiEnd));
            trial(t).stims.tmpfrq = 0; 
            
            % determine velocity of the phase shift
            phaVelDS = NaN;  % in dva/s
            if design.stiPres(cond) > 0
                if design.stiPres(cond) == 1; Am = trial(t).sac.amp; end
                if design.stiPres(cond) == 2; V0 = ms.mnsq.data(2);A0 = ms.mnsq.data(3);Am = ms.mnsq.data(4); end
                phaVelDS = V0 * (1 - exp(-Am / A0)); % in dva/s
                
                % Correct phase shift velocity if it is lower than the default
                if phaVelDS < design.phaVelDS; phaVelDS = design.phaVelDS; end
                phaVelPF = phaVelDS*scr.ppd*scr.fd; % in pix/fra
            end 

            % calculate the evolution of the velocity of the phase shift
            % (here constant)
            trial(t).stims.evovel = repmat(velVec*phaVelDS,design.numStim,1) + repmat(trial(t).stims.tmpfrq ./ trial(t).stims.pars.frq',1,length(velVec)); % desired stimulus velocity [dva per sec]

            % define phase change per frame for entire profile
            phaFra = scr.fd*trial(t).stims.evovel.*repmat(trial(t).stims.pars.frq'*360,1,length(velVec));  % phase change per frame [deg  frame]
            
            % define stimulus visibility and velocity
            trial(t).stims.vis = zeros(design.numStim,trial(t).totNFr);
            trial(t).stims.pha = zeros(design.numStim,trial(t).totNFr);
            trial(t).stims.vis(:,stiBeg:stiEnd) = design.stiPres(cond) > 0;
            trial(t).stims.pha(:,stiBeg:stiEnd) = cumsum(phaFra,2);        % in [deg / frame]
           
            % add random phase to each stimulus
            trial(t).stims.pha = trial(t).stims.pha + repmat(360* rand(design.numStim,1),1,trial(t).totNFr); % in [dpc / frame]
            
            % recalculate phase shift in pixels per frame
            trial(t).stims.pha = trial(t).stims.pha*(scr.ppd/(trial(t).stims.pars.frq'*360));    % in [pix / frame]
            % trial(t).stims.pha1 = cumsum(repmat(phaVelDS*scr.ppd*scr.fd,1,trial(t).totNFr)) + rand(design.numStim,1);*scr.ppd/trial(t).stims.pars.frq
            
            % make sure the sine grating is only shifted by its size in the
            % horizontal plane (i.e. the extent of the screen plus 1 phase
            % [in pix].
            f = scr.resX + 1/trial(t).stims.pars.frqp;
            trial(t).stims.pha = mod(trial(t).stims.pha,f);               % in [pix / frame]
            
            % define modulation of contrast across time
            ampVec = ones(1,length(stiBeg:stiEnd));
            ramVec = normcdf(1: trial(t).ramNFr, trial(t).ramNFr/2, trial(t).ramNFr/6);
            ampVec(1: trial(t).ramNFr) = ramVec;
            ampVec(end:-1:(end- trial(t).ramNFr+1)) = ramVec;
            trial(t).stims.evoamp = repmat(trial(t).stims.pars.amp',1,length(ampVec)) .* repmat(ampVec,design.numStim,1);
            trial(t).stims.amp = zeros(design.numStim,trial(t).totNFr);
            trial(t).stims.amp(:,stiBeg:stiEnd) = trial(t).stims.evoamp;

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %         response & feedback information         %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            trial(t).scale = design.scale;
            sRand = round(size(trial(t).scale.lvl(3).a.labels,2)*rand);                               % random onset position of the scale
            trial(t).scale.line.pos = trial(t).scale.line.posP + trial(t).fixa.loc(1:2)';             % center scale on fixation dot
            trial(t).scale.hand.posvecP = trial(t).scale.hand.posvecP + trial(t).fixa.loc(1:2)';      % center response promt on fixation dot.
            for i= 1:length(trial(t).scale.lvl)                                
                trial(t).scale.lvl(i).q.posP = trial(t).scale.lvl(i).q.posP + trial(t).fixa.loc(1:2); % center question text on fixation dot.
                trial(t).scale.lvl(i).a.posP = trial(t).scale.lvl(i).a.posP + trial(t).fixa.loc(1:2); % center response options on fixation dot.
            end
            if rand < 0.5, trial(t).scale.bandColV = visual.white - trial(t).scale.bandColV; end      % revert scale direction in 1/2 of the trials.
 
            % Block name messages (per the 4 different conditions)
            if setting.train, trial(t).message = sprintf('This is a training block (with feedback)');
            else trial(t).message = sprintf(''); end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % define critical events during stimulus presentation %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            trial(t).events = zeros(1,trial(t).totNFr);
            trial(t).events(stiBeg)                 = 1;
            trial(t).events(ctrBeg)                 = 2;
            trial(t).events(appBeg(~isnan(appBeg))) = 3;
            trial(t).events(appTop(~isnan(appTop))) = 4;
            trial(t).events(ctrEnd)                 = 5;
            trial(t).events(stiEnd)                 = 6;

            % time requirements for responses
            trial(t).maxSac = design.timMaSa;
            trial(t).aftKey = design.timAfKe;

            % store trial features
            trial(t).feedback = setting.train;                  % 1 = feedback, 0 = no feedback
            trial(t).expcon   = cond;                           % 1 = no stimulus, simulated microsaccade condition, active microsacccade condition
            
            % store stimulus features
            trial(t).fixpox = fixx;                             % x coordinate of position of fixation [dva rel. to midpoint]
            trial(t).fixpoy = fixy * -1;                        % y coordinate of position of fixation FLIPPED [dva rel. to midpoint] 
            trial(t).phavel = phaVelDS;                         % velocity of the phase shift [dva/s]
            trial(t).stiori = 180 - trial(t).stims.pars.ori;    % stimulus orientation FLIPPED BACK to saccade coordinates: 0 = vertical, phase shift dir. right, 180 = vertical, phase shift dir.left
            trial(t).appbeg = appBeg * scr.fd * 1000;           % time point at which apperture shift starts [ms]
            trial(t).apptop = appTop * scr.fd * 1000;           % time point at which apperture shift reaches max velocity [ms]
            trial(t).appdur = appdur * scr.fd * 1000;           % duration of the displayed microsaccade [ms]
            trial(t).appmdX = appmdX;                           % x coordinate of position shift of the apperture [dva]
            trial(t).appmdY = appmdY * -1;                      % y coordinate of position shift of the apperture FLIPPED BACK to saccade coordinates [dva]
            trial(t).appori = round(rad2deg(atan2(trial(t).appmdY,trial(t).appmdX))); % appeture shift orientation FLIPPED BACK to saccade coordinates [deg]   
            trial(t).appvpk = appvpk;                           % peak velocity of the aperture movement [dva/s]
            trial(t).appamp = sqrt(appmdX^2 + appmdY^2);        % amplitude of the position shift of the apperture (here the difference in coordinate systems does not matter) [dva]

            % store some scale features
            trial(t).sOnPos = sRand;                         % onset position of scale hand [frame idx]
            
            %%%%%%%%%%%%%%%%%%%%%%%
            % draw loading screen %
            %%%%%%%%%%%%%%%%%%%%%%%
            drawLoadingScreen(visual.scrCenter, 5, c, totNT);
        end
    end
    r = randperm(t);                        % randomise the trials per block
    if ~setting.train
        design.b(:,b).train = [];
        design.b(:,b).trial = trial(r);     % randomise in which order the condtions are presented (in experiment)
    elseif setting.train
        design.b(:,b).train = trial(r);     % randomise in which order the condtions are presented (in training)
        design.b(:,b).trial = [];
    end
end

design.nBlocks = b;                             % this number of blocks is shared by all qu
design.blockOrder = randperm(b);                % org: design.blockOrder = 1:b;

design.nTrialsPB = t;                           % number of trials per Block

% strictly for testing: Emulation of propixx on low res screen: 
if setting.Pixx == 2; scr.refr = 60; scr.fd = 1/scr.refr; setting.Pixx = 0; end
 
save(sprintf('%s.mat',subjectCode),'design','visual','scr','keys','setting');
