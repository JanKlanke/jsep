function drawLoadingScreen(loc, siz, c, tot)
%
% 2020 by Jan Klanke
% 2021 by Jan Klanke 
% 
% Input:  loc     - mid-pint of the loadnig-status circle
%         siz     - size of the loadnig-status circle
%         c       - trialcounter
%         tot     - total no. of tials       

global setting scr visual

pixx = setting.Pixx;

% calculate onscreen location of circle:
location = loc + round(scr.ppd * [-siz -siz siz siz]);

% draw filled circle segment (like a pie chart of %-done):
Screen('FillRect', scr.myimg, visual.bgColor);
Screen('FillArc', scr.myimg,0.3,location,0,c*(360/tot) + (360/tot));

% draw text
drawText(sprintf('%i%s of trials created',round(100*c/tot),'%'),scr.centerX + scr.ppd*siz + 5,scr.centerY + scr.ppd*siz/2);

% flip to screen:
flip = 0;
if pixx, flip = PsychProPixx('QueueImage', scr.myimg);
else     flip = Screen('Flip', scr.myimg); end

% fill up buffer if necessary
if c == tot
    while flip == 0
        flip = PsychProPixx('QueueImage', scr.myimg);
    end
    for i = 1:12
        Screen('FillRect', scr.myimg, visual.bgColor);
        Screen('FillArc',  scr.myimg, 0.3, location, 0, 360);
        drawText(sprintf('%i%s of trials created', 100, '%'), scr.centerX + scr.ppd*siz + 5,scr.centerY + scr.ppd*siz/2);
        if pixx, PsychProPixx('QueueImage', scr.myimg);
        else     Screen('Flip', scr.myimg); end
    end
end
end
        