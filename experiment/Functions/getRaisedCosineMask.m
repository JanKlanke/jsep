function sti = getRaisedCosineMask(par)
% 2021 by Martin Rolfs
% 2021 by Jan Klanke
% 
% Input:  par - parameters that are needed for stimulus (i.e. sine
%               grating and the raised cosine mask)
%
% Output: sti - Matrix (sineGrating : sineGrating : 2D-RaisedCosineMask)

global scr 

%%%%%%%%%%%%%%%%%%%%%%%
% Define sine grating %
%%%%%%%%%%%%%%%%%%%%%%%

% Calculate parameters of the grating:
ppc = ceil(1/par.frqp)*2;                 % pixels/cycle, rounded up.
sizX = scr.resX + ~mod(scr.resX,2);       % extent of the grating in the horizontal plane (equal to 2*screen size in pix).
sizY = par.sizp/2 + ~mod(par.sizp/2,2);   % siz in pixels, rounded up (if necessary)

% Create one single static grating image:
X = meshgrid(-sizX:sizX + ppc,-sizY:sizY);
grating = scr.bgColor + scr.bgColor*par.amp*cos(X*par.frqp*2*pi + par.pha);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define raised cosine mask %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% outer part
sizeOut = par.sizp - mod(par.sizp,2);                      % outer radius in pixels (make sure its an even number)
ROut    = par.tap;                                         % length of the taper section (outer)

% inner part
sizeInn = round(par.sizp/2) - round(mod(par.sizp/2,2));    % inner radius in pixels (make sure its an even number)
RInn    = par.tap * (sizeOut/sizeInn);                     % length of the taper section (inner)

% get raised cosine Functions for differnt parts of the filter 
baseOut = raisedCos2D(sizeOut,ROut);
baseInn = raisedCos2D(sizeInn,RInn);

% combine different parts of the filter
baseInnCompl = ones(sizeOut,sizeOut);
baseInnOfOut = sizeOut/2+1+(-sizeInn/2:(sizeInn/2-1));
baseInnCompl(baseInnOfOut,baseInnOfOut) = 1-baseInn;
baseCombined = baseOut+baseInnCompl;

% subtract white b/c filter spanns from 1-2 and we need it to span between
% 0 and 1.
mask =  baseCombined - scr.white;

% store pulse-shaped filter mask in 2nd channel of grating.
% grating = [];
grating(:,:,2) = 0;
grating(1:par.sizp-mod(par.sizp,2), 1:par.sizp-mod(par.sizp,2), 2) = mask;

% make grating the ouput stimulus
sti = grating;
end

% function that calcualtes the raised fading 
function base = raisedCos2D(siz,R)

base = zeros(siz,siz);
tukey = tukeywin(siz,R);
tukey = tukey(siz/2:siz);
x = linspace(-siz/2, siz/2, siz);
y = linspace(-siz/2, siz/2, siz);
for i=1:1:siz
    for j=1:1:siz
        if (round(sqrt(x(i)^2 + y(j)^2)) <= siz/2)
            base(i,j) = tukey(round(sqrt(x(i)^2+y(j)^2)));
        end
    end
end
end
