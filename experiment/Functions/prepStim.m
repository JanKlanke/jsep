function prepStim
%
% 2008 by Martin Rolfs
% 2021 by Jan Klanke


global visual scr

% define Colors
visual.black   = BlackIndex(scr.main);
visual.white   = WhiteIndex(scr.main);
visual.bgColor = (visual.black+visual.white)/2;     % background color
visual.fgColor = visual.black;                      % foreground color
visual.inColor = visual.white-visual.bgColor;       % increment for full contrast

% define spatial parameters
visual.ppd       = dva2pix(1,scr);            % pixel per degree
visual.dpp       = pix2dva(1,scr);            % degree per pixel
visual.scrCenter = [scr.centerX scr.centerY scr.centerX scr.centerY];
visual.fixCkRad  = round(1.5 * visual.ppd);   % fixation check radius
visual.fixCkCol  = visual.black;              % fixation check color

% boundary radius at saccade target
visual.boundRad = visual.fixCkRad;

% nyquist frequency
visual.fNyquist = 0.5;

% create special donut-shaped stimulus
visual.procStimsPar = getStimulusPars;
visual.procStimsMat = getRaisedCosineMask(visual.procStimsPar);
visual.procStimsTex = createStimulusShader(visual.procStimsMat);

% get priority of window activities to maximum
if IsLinux
    scr.priorityLevel = 1;
else
    scr.priorityLevel = MaxPriority(scr.main);
end
