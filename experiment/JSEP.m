%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Judgement: Self vs. extraneously caused percept   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 2021 by Jan Klanke
%
% 
% DESKRIPTION
%
%
%
% TODOs
%
%
%

clear all;
clear mex;
clear functions; 
delete(instrfindall);

addpath('Functions/','Data/','Data/msCoordinates/','Data/mainSequences/');

home;
expStart=tic;


global setting visual scr keys re %#ok<*NUSED>

setting.TEST = 2;         % test in dummy mode? 0=with eyelink; 1=mouse as eye; 2=no gaze position checking of any sort
setting.sloMo= 1;         % To play stimulus in slow motion, draw every frame setting.sloMo times
setting.train= 0;         % do you want to run a pracitce block?
setting.Pixx = 1;         % Are you using the propixx (1) or not (0) - do you want to simulate the propixx (2)
setting.knob = 0;         % do you want to use the rotation knob for the response mechanism (1) or not (0)
setting.sdir = 0;         % direction of the response schema: 0 = yes->no; 1= no->yes

exptname = 'JSEP';

try 
    newFile = 0;
    while ~newFile
        [vpno, seno, cond, coow, dome, subjectCode] = prepExp(exptname);
        subjectCode = strcat('Data/',subjectCode);
        
         
        % create data file
        datFile = sprintf('%s.dat',subjectCode);
        if exist(datFile,'file')
            o = input('>>>> This file exists already. Should I overwrite it [y / n]? ','s');
            if strcmp(o,'y')
                newFile = 1;
            end
        else
            newFile = 1;
        end
    end
    
    % prepare replay condition
    start = 1;
    while start       
        [ms, start, abort] = prepReplayCondition(vpno, seno, coow, start);
    end
        
    % prepare screens
    prepScreen;
   
    % get key assignment
    getKeyAssignment;
    
    % initiate rotation know (if necessary)
    if setting.knob; re = RotaryEncoder('/dev/ttyS101'); end   
    
    % disable keyboard
    ListenChar(2);
          
    % prepare stimuli
    prepStim;
    
    % generate design
    design = genDesign(vpno, seno, cond, dome, ms, subjectCode);
    
    % initialize eyelink-connection
    if setting.TEST<2
        [el, err]=initEyelinkNew(subjectCode(6:end));
        if err==el.TERMINATE_KEY
            return
        end
    else
        el=[];
    end
    
    % runtrials
    design = runTrials(design,datFile,el);
    
    % shut down everything
    reddUp;
    
catch me
    rethrow(me); 
    reddUp; %#ok<UNRCH>
end

expDur=toc(expStart);

fprintf(1,'\n\nThis (part of the) experiment took %.0f min.',(expDur)/60);
fprintf(1,'\n\nOK!\n');

% plotReplayResults(exptname, vpno, seno);