function [p] = plotReplayResults(expname, vpno, seno)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% session info
vp = double(vpno) - 96;
sn = str2num(seno);

% load data
% get all files 
files = dir(sprintf('Data/%s_*.dat', expname));

% load data of participant
for i= 1: size(files,1)
    if ~contains( files(i).name, 'x' ) && ~( ( contains( files(i).name, 'y' ) && contains( files(i).name, '0' ) ) )
        tdat = importdata(files(i).name);        % load data

        pID  = cellfun(@(x) double(x)-96, tdat.textdata(:,1));
        sbt  = str2double(tdat.textdata(:,2:4)); % convert tID data (seno, block, trial)
        ipd = horzcat(pID, sbt, tdat.data);      % concatenate tID and trial data
        ipdata(i) = {ipd};                       % put data in cell structure
    end
end
data = vertcat(ipdata{:});                       % concatenate vertically into one data table

% get session indices 
[cmb,~,idx] = unique(data(:,1:2), 'rows');
df_session = data(  (idx == find( cmb(:,1) == vp & cmb(:,2) == sn )),: );
df_allelse = data( ~(idx == find( cmb(:,1) == vp & cmb(:,2) == sn )),: );

% do % for session.
v1 = [nanmean(df_session(df_session(:,5) == 2 | df_session(:,5) == 4,38)), ...
      nanmean(df_session(df_session(:,5) == 3 | df_session(:,5) == 5,38))];
v2 = [nanmean(df_session((df_session(:,5) == 2 | df_session(:,5) == 4) & df_session(:,38) == 0,39 )), ...
      nanmean(df_session((df_session(:,5) == 3 | df_session(:,5) == 5) & df_session(:,38) == 1,39 ))]; 
v3 = [nanmean(df_session((df_session(:,5) == 2 | df_session(:,5) == 4) & df_session(:,38) == 1,39 )), ...
      nanmean(df_session((df_session(:,5) == 3 | df_session(:,5) == 5) & df_session(:,38) == 0,39 ))];  

% do % for all sessions.
s1 = [nanmean(df_allelse(df_allelse(:,5) == 2  | df_allelse(:,5) == 4,38)),...
      nanmean(df_allelse(df_allelse(:,5) == 3  | df_allelse(:,5) == 5,38))];
s2 = [nanmean(df_allelse((df_allelse(:,5) == 2 | df_allelse(:,5) == 4) & df_allelse(:,38) == 0, 39)),...
      nanmean(df_allelse((df_allelse(:,5) == 3 | df_allelse(:,5) == 5) & df_allelse(:,38) == 1, 39))];
s3 = [nanmean(df_allelse((df_allelse(:,5) == 2 | df_allelse(:,5) == 4) & df_allelse(:,38) == 1, 39)),...
      nanmean(df_allelse((df_allelse(:,5) == 3 | df_allelse(:,5) == 5) & df_allelse(:,38) == 0, 39))];


% mix colors
c = [0 128 255; 255 153 51]/255;
xlim = [0.5, 1.5; 1.5, 2.5];

% draw plot
p = figure;
set(gcf, 'position', get(0, 'ScreenSize'));
hold on
suptitle( sprintf('Participant %s%s Results\n\n', vpno, seno) );
subplot(1,3,1);
hold on
% plot stuff for estimate
% title( 'Percentage correct' );
for i= 1:length(v1); b= bar(i,v1(i)); set(b, 'FaceColor',c(i,:)); 
    l= plot(xlim(i,:),[s1(i) s1(i)], 'k--', 'LineWidth', 2); t= text( i +.25, s1(i) + 0.015 , 'meanAll' );
end

% (re-) define axes
xticks([1, 2]); ylim([0,3]);
xticklabels({'Repaly condition', 'Active condition'});
yticks([0, 1]); ylim([0,1]);
ylabel( design.b(1).trial(1).scale.lvl(1).q.labels );
yticklabels({'No.', 'Yes.'});
hold off

subplot(1,3,2);
hold on
% plot stuff for Likert scale
% title( 'Sureness of estimate (correct responses)' );
for i= 1:length(v2); b= bar(i,v2(i)); set(b, 'FaceColor',c(i,:)); 
    l= plot(xlim(i,:),[s2(i) s2(i)], 'k--', 'LineWidth', 2); t= text( i+.25, s2(i) + 0.015 , 'meanAll' );
end

% (re-) define axes
xticks([1, 2]); ylim([0,3]);
xticklabels({'Repaly condition', 'Active condition'});
yticks([0, 0.5, 1]); ylim([0,1]);
ylabel( design.b(1).trial(1).scale.lvl(2).q.labels );
yticklabels({'No.', 'Yes.'});
hold off

subplot(1,3,3);
hold on
% plot stuff for Likert scale
title( 'Sureness of estimate' );
for i= 1:length(v3); b= bar(i,v3(i)); set(b, 'FaceColor',c(i,:)); 
    l= plot(xlim(i,:),[s3(i) s3(i)], 'k--', 'LineWidth', 2); t= text( i+.25, s3(i) + 0.015 , 'meanAll' );
end

% (re-) define axes
xticks([1, 2]); ylim([0,3]);
xticklabels({'Repaly condition', 'Active condition'});
yticks([0, 0.5, 1]); ylim([0,1]);
ylabel( sprintf('How sure are you?'));
yticklabels({'Not sure.', 'Sure.','Very sure.'});
hold off

hold off
