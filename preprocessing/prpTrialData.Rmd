---
title: "Preprocessing Script 2: Trial data parser."
author: Jan Klanke
date: "`r Sys.date()`"
output: 
  pdf_document: default
---

```{r setup, include= FALSE}
knitr::opts_chunk$set( include= TRUE, size= 'footnotesize' )
knitr::opts_knit$set(  root.dir= '/Users/jan' )

# Change hook to be able to modify font size in chunks
def.source.hook <- knitr::knit_hooks$get( 'source' )
knitr::knit_hooks$set( source= function( x, options ) {
  x <- def.source.hook( x, options )
  ifelse( !is.null( options$size ), 
         paste0( '\\', options$size, '\n\n', x, '\n\n \\normalsize' ), 
         x ) } )
```

This script parses the trial data stored typically in text format in MSG data files. There are three kinds of data in the msg files: a) data related to the eye tracking (in the script called 'dataTrack'), b) data relating to the output of each experimental trial ('dataTrial'), and c) the timestamps at which crucial messages (f.e. 'stimulus on', etc.) has been sent.

# Parameter
This chunk contains parameters such as experiment and a list of names for the columns (etc).
```{r}
# Name of the experiment.
expname <- 'JSEP' 

# Specify appropriate column names for MSG file table.
colMSG <- c( 'vpno', 'seno', 'block', 'trial', 'domEye',
             'expcon', 'train',
             'fixpox', 'fixpoy', 'stiori', 'phavel', 'appbeg', 'apptop', 'appdur', 'appmovdX', 'appmovdY', 'appori', 'appvkp', 'appamp',
             'dispSacIdx', 'dispSacReq', 'dispSacAmp', 'dispSacVPk', 'dispSacDur', 'dispSacAng',
             'scaleDir', 'scaleOnPosDeg',
             'tStimOn', 'tStimCf', 'tStimMS', 'tStimMT', 'tStimCd', 'tStimOf', 'tRes', 'tClr',
             'perceptualResponse', 'microsaccadeResponse',  'certaintyResponse',               
             'tLoopFrames', 'totNFr' )

# Indices of the columns with VP number, session number, block number, and trial number.
# Necessary to create a unique trial ID!
identifier <- c( 'vpno', 'seno', 'block', 'trial' )

# List with crucial event flags.
crucialFlags <- c( 'StimOn', 'StimCf', 'StimMS', 'StimMT', 'StimCd', 'StimOf')  

# Index of flag that is supposed to be used as a reference 
# (i.e. the time point that is supposed to be the 0 value in each trial).
refFlag <- 'StimOn'

# Flag to indicate whether all files in the data folder should be processed or only new ones.    
reEvalAll <- 'no' 
```

# Folder & format spec. parameter
This chunk entails all the parameter for the folder structure, file format specifiers, and so on.
```{r}
# Directory of subfolder with data. 
fld <- list()
fld$dataRepo <- './Seafile/Data/'                                     
fld$raw   <- paste0( fld$dataRepo, expname, '/raw' )                     
fld$trial <- paste0( fld$dataRepo, expname, '/preprocessed/trialData' ) 
fld$track <- paste0( fld$dataRepo, expname, '/preprocessed/trackData/trackFlags' ) 

# Directory of subfolder with functions for MS detection 
fld$functions <- paste0( './', expname, '/preprocessing/functions/' ) 

# Format specs of input and ouptu data (likely no need for change!).
fS <- list()
fS$msg <- '.msg'     
fS$dat <- '.dat'     
fS$csv <- '.csv'     
```

# Load libraries & sources
These are the libraries that are needed in the script. The libraries are essentially the tidyverse and vroom for fast and efficient data loading. External functions are not needed before the next step of the data preprocessing.
```{r include= FALSE}
# Load libraries.
libraries <- c( 'data.table', 'vroom', 'tidyverse'  )
lapply( libraries, require, character.only= TRUE )
```

# Load MSG files
After checking whether all files should be (re-) processed or just new ones, the following chunk loads in the corresponding MSG files. 
```{r}
# Get those file that are already processed and change their format spec. to MSG.
fList <- list()
fList$trial <- list.files( fld$trial, paste0( '*', fS$csv ) ) %>% str_replace( fS$csv, fS$msg )

# Get names of MSG files from data dir. and check if they have been processed before (and or are supposed to be processed again).
fList$msg <- list.files( fld$raw, paste0( '*', fS$msg ), full.names= TRUE )
if( reEvalAll != 'yes' & !is_empty( fList$trial ) ) {
  fList$msg <-  str_subset( fList$msg, paste( fList$trial, collapse= '|' ), negate= TRUE )  }

# Load MSG files.
vroom( fList$msg, col_names= 'Txt', delim= '\t',  id= 'fName' ) %>%                # - load files
  mutate_at( vars( fName ), list( ~str_remove_all( ., paste0( fld$raw, '/|' , fS$msg ) ) ) ) %>%    # - erase unnecessary info from file name
  separate( Txt, into= c( 'Flag', 'TimeStamp', 'Txt' ), sep= '\\s', extra= 'merge' ) %>%   # - separate into flag, time stamp, and text columns
  filter( str_detect( Flag, 'MSG' ) ) %>%                                          # - filter relevant tracking messages only
  select( -Flag ) -> msgData                                                       # - erase irrelevant MSG flag 
```

# Prepare track data
In this this chunk, all rows are filter that are marked by the eyelink system as tracking data ('!CAL'). They are split in different components, and the irrelevant column is dropped. 
```{r}
# Create trackdata.
filter( msgData, grepl( '!CAL', Txt ) ) %>%                                           # - filter column for relevant entries (i.e. Subject code & !CAL msgs)
  separate( Txt, into= c( 'Flag', 'Msg' ), sep= '\\s', extra= 'merge' ) -> dataTrack  # - separate these msgs into timestamps and rest                 
```

# Rehash data (i.e. separate TStPs and Msgs and keep only necessary items)
In this chunk, the rows with crucial information (i.e., stimulus flags and trial data) are selected and separated into different columns. The irrelevant column is then erased as well as the irrelevant part of each stimulus flag ('EVENT_').
```{r message= FALSE, warning= FALSE}
# Create raw MSG data 
filter( msgData, str_detect( Txt, paste( c ( crucialFlags, 'TrialData' ), collapse='|' ) ) ) %>% # - filter Msg column for relevant entries 
  separate( Txt, into= c( 'Flag', colMSG ), sep=  '[ \\t]' ) %>%                                 # - split Msg into defined columns
  mutate( Flag= str_remove( Flag, '.*_' ) ) %>%                                                  # - remove unnecessary 'EVENT_' tags
  mutate_at( vars( identifier[2:3] ), list( ~sprintf( '%02d', as.numeric(.) ) ) ) %>%            # - bring identifier columns in user-friendly format
  mutate_at( vars( identifier[4] ), list( ~sprintf( '%03d', as.numeric(.) ) ) ) %>%
  mutate( tID= ifelse( Flag == 'TrialData',  paste0( !!!syms( identifier ) ), NA ) ) %>%         # - create tID column that uses information from id columns
  fill( tID, .direction= 'up' ) -> dataTrial_raw                                                 # - fill tID column with correct information 
```

# Bring time data from column to row format and recalculate timeData in ms
Here, the time points are calculated at which the flags have been sent (ms). This is done by subtracting the column with the reference times from all other columns. Subsequently, the rows are brought back into the order of trial (i.e. group by participant, session, block and trial [1-> end]). Remaining are the columns with the time stamps of the crucial events per trial. 
```{r}
# Get reference Timestamps.
group_by( dataTrial_raw, tID ) %>% 
  filter( !duplicated( Flag, fromLast= TRUE ), Flag == refFlag ) %>%                                    # - filter out time stamps from incomplete trials
  select( fName, tID, TimeStamp ) %>%                                                                   # ... and mark the ones referenced by 'refFlag'. 
  mutate( refFlag= 'rTS' ) -> referenceTS

# Get the crucial timestamps of all complete trials.
left_join( dataTrial_raw, referenceTS, by= c( 'fName', 'tID', 'TimeStamp' ) ) %>%                       # - join raw trial data /w refelence time stamps
  group_by( tID ) %>% 
  fill( refFlag, .direction= 'down' ) %>%                                                               # - Fill down(!) so as to filter out flags for aborted trials!
  select( fName, tID, TimeStamp, Flag, refFlag ) %>%                                                    # - select appropriate time points
  filter( refFlag == 'rTS', !( Flag == 'TrialData' ) ) %>%                                              # - filter for rows marked as rTS but NOT(!) marked as trial data
  pivot_wider( id_cols= c( fName, tID), names_from= Flag, values_from= TimeStamp ) -> dataTime_ref      # - change from long to wide format
 
# Recalculate timestamps as msecs.
left_join( dataTime_ref, select( referenceTS, -refFlag ), by= c( 'fName', 'tID' ) ) %>%                        # - create column to subtract from the rest
  mutate_at( vars( all_of( c( crucialFlags, 'TimeStamp' ) ) ), list( ~as.numeric( as.character( . ) ) ) ) %>%  # - change columns with time stamps to numeric values
  mutate_at( vars( all_of( crucialFlags ) ), list( ~. - TimeStamp ) ) %>%                                      # - subtract the reference column from all other columns
  select( tID, TimeStamp, all_of( crucialFlags ) ) %>%                                                         # - select relevant columns in correct order
  rename_at( vars( c( all_of( crucialFlags ), TimeStamp ) ), list( ~c( paste0( 'tedf_', crucialFlags ), 'refTimeStamp' ) ) ) -> dataTime
```

# Create trial data
Here the columns with the trial data are selected.
```{r}
# Create data subset that only contains trial data.
filter( dataTrial_raw, Flag == 'TrialData' ) %>% # - filter for rows marked as trial data
  select( -Flag, -TimeStamp ) -> dataTrial       # - remove columns that are not needed
```

# Prepare file for output & store data
In the following chunk, trial and time data tibbles are prepared for storage. First appropriate file names are created for each participant and session file. Afterwards, the data tibbles are separated into different list elements per participant and session. These are then stored as individual csv files in the target directory. Similarly, the track data is stored in individual csv files per participant and session. 
```{r include= FALSE}
# Create names for the output trial and track data.
list( track= paste0( fld$track, '/', unique( dataTrack$fName ), fS$csv ),
      trial= paste0( fld$trial, '/', unique( dataTrial$fName ), fS$csv ) ) -> name

# Combine trial and time data and split them into different files per vp and session.
group_by( dataTrack, fName ) %>% group_split( keep= FALSE ) -> dataTrack_fin

# Combine trial and time data and split them into different files per vp and session.
left_join( dataTrial, dataTime, by= 'tID' ) %>% filter( !is.na( vpno ) ) %>% group_by( fName ) %>% group_split( keep= FALSE ) -> dataTrial_fin

# Write results to file.
map2( .x= dataTrack_fin, .y= name$track, .f= ~vroom_write( x= .x, path= .y, delim= ',', col_names= TRUE ) )
map2( .x= dataTrial_fin, .y= name$trial, .f= ~vroom_write( x= .x, path= .y, delim= ',', col_names= TRUE ) )
```